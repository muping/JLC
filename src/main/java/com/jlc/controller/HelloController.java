package com.jlc.controller;
import com.jlc.common.exception.ResponseResult;
import com.jlc.core.build.*;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {

    @RequestMapping("/hello")
    public ResponseResult hello(String code) {
        JavaBuild j=new JavaBuild();
        j.getRunningResult(code);
        return ResponseResult.generateResponseResult(code);
    }

    public static void main(String[] args){
        String javaCode ="java:public class Main {\n" +
                "    public static void main(String[] args) {\n" +
                "        for(int i=0;i<1;i++){\n" +
                "                   System.out.println(\"Hello Java!\"); \n" +
                "        }\n" +
                "    }\n" +
                "}";
        String cPlusCode="c++:#include <iostream>\n" +
                "using namespace std; \n" +
                "int main() {\n" +
                "    cout << \"Hello C++!\\n\";\n" +
                "}\n";

        String cCode="c:#include<stdio.h>\n" +
                "\n" +
                "int main(void) {\n" +
                "    puts(\"Hello C!\");\n" +
                "    return 0;\n" +
                "}";

        String pythonCode="python:print(\"Hello python!\")";

        System.out.println(new Builder(pythonCode).getResult());
        System.out.println(new Builder(javaCode).getResult());
        System.out.println(new Builder(cCode).getResult());
        System.out.println(new Builder(cPlusCode).getResult());

    }
}
