package com.jlc.core.build;

public class CBuild extends BaseBuild{

    private static final String FILE_NAME="code.c";

    @Override
    public String getFileName(String code) {
        return FILE_NAME;
    }

    @Override
    public String[] getCommand() {
        String name=FILE_NAME.substring(0,FILE_NAME.lastIndexOf(".c"));
        String[] command={
                "cmd","/c",
                "gcc","-o",name,FILE_NAME,
                "&&",name
        };
        return command;
    }
}
