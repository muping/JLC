package com.jlc.core.build;

public class CPlusBuild extends BaseBuild{

    private static final String FILE_NAME="code.cpp";
    @Override
    public String getFileName(String code) {
        return FILE_NAME;
    }

    @Override
    public String[] getCommand() {
        String name=FILE_NAME.substring(0,FILE_NAME.lastIndexOf(".cpp"));
        String[] command={
                "cmd","/c",
                "g++","-o",name,FILE_NAME,
                "&&",name
        };
        return command;
    }
}
