package com.jlc.core.build;

public class Builder {

    private BaseBuild build;
    private String code;

    public Builder(String code) {
       String language=code.substring(0,code.indexOf(":"));
       this.code=code.substring(code.indexOf(":")+1);
       if("c".equals(language)){
           build=new CBuild();
       }else if("c++".equals(language)){
           build=new CPlusBuild();
       }else if("java".equals(language)){
           build=new JavaBuild();
       }else if("python".equals(language)){
           build=new PythonBuild();
       }else{
           build=null;
       }
    }

    public  String getResult(){
        if(build==null)
            return null;
        return build.getRunningResult(code);
    }

}
