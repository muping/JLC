package com.jlc.core.build;
import org.apache.commons.io.FileUtils;
import java.io.*;
public abstract class BaseBuild implements IBuild {

    //最大文件夹数量
    private static final int MAX_FOLDER_COUNT=10000;

    //服务器cmd的编码格式,根据电脑cmd编码实际情况修改
    private static final String CMD_ENCODING="gbk";

    //运行结果
    private String result;

    //目录计数
    private static int directoryCount=0;

    //目录名
    private String directoryName;

    //文件名
    private String fileName;

    //获得运行结果
    public String getRunningResult(String code){
        //获得目录名
        directoryName=getTempDirectoryName();
        //获得文件名
        fileName=getFileName(code);
        //创建文件
        createFile(code);
        //编译运行
        buildAndRun(getCommand());

        Thread t=new Thread(new DeleteFileThread());
        t.start();
        //返回结果
        return result;
    }

    private void createFile(String code){
        code=code.replace("\n","\r\n");
        try {
            FileUtils.write(new File(directoryName+fileName),code,CMD_ENCODING,false);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //编译并运行，将运行结果存入result中
    private void buildAndRun(String[] commands) {
        String line;
        StringBuilder output = new StringBuilder();
        StringBuilder error = new StringBuilder();
        try {
            ProcessBuilder builder =new ProcessBuilder(commands);
            //builder.redirectErrorStream(true);
            builder.directory(new File(directoryName));
            Process process = builder.start();
            InputStreamReader in = new InputStreamReader(process.getInputStream(),CMD_ENCODING);
            BufferedReader bufferedReader = new BufferedReader(in);
            while ((line = bufferedReader.readLine()) != null) {
                output.append(line + "\n");
            }
            in.close();
            in=new InputStreamReader(process.getErrorStream(),CMD_ENCODING);
            bufferedReader=new BufferedReader(in);
            while ((line = bufferedReader.readLine()) != null) {
                error.append(line + "\n");
            }
            in.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        if(error.length()==0){
            result="SUCCESS:\n"+output;
        }else{
            result="ERROR:\n"+error;
        }
    }

    private synchronized String getTempDirectoryName(){
        if(directoryCount++>MAX_FOLDER_COUNT)
            directoryCount=0;
        return "temp/code"+directoryCount+"/";
    }

    //用于删除文件的线程
    class DeleteFileThread implements Runnable{

        private static final int delay=2000;
        @Override
        public void run() {
            try {
                Thread.sleep(delay);
                FileUtils.deleteDirectory(new File(directoryName));
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
