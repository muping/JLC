package com.jlc.core.build;

public class JavaBuild extends BaseBuild {

    //文件名
    private static String fileName = null;

    @Override
    public String getFileName(String code) {
        //TODO

        if(fileName==null){
            fileName="Main.java";
        }
        return fileName;
    }

    @Override
    public String[] getCommand() {
        String[] command={
                "cmd","/c",
                "javac",fileName,
                "&&","java",fileName.substring(0,fileName.lastIndexOf(".java"))
        };
        return command;
    }
}


