package com.jlc.core.build;

import java.util.List;

//编译运行Code的接口
public interface IBuild {

    //获取文件名
    String getFileName(String code);

    //获取将要执行的命令
    String[] getCommand();


}
