package com.jlc.core.build;

public class PythonBuild extends BaseBuild{

    private static final String FILE_NAME="code.py";

    @Override
    public String getFileName(String code) {
        return FILE_NAME;
    }

    @Override
    public String[] getCommand() {
        String[] command={
                "cmd","/c",
                "python",FILE_NAME,
        };
        return command;
    }
}
