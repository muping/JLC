package com.jlc.dal.entity.enums;

/**
 * 用户类别枚举
 */
public enum UserKindEnum {

    USER("普通用户"),
    MANAGER("管理员");

    private String des;

    UserKindEnum(String des) {
        this.des = des;
    }

    @Override
    public String toString() {
        return this.des;
    }
}
