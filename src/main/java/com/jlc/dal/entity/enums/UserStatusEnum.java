package com.jlc.dal.entity.enums;

/**
 * 用户状态枚举
 */
public enum UserStatusEnum {

    NORMAL("正常"),
    LOCK("锁定");

    private String des;

    UserStatusEnum(String des){
        this.des=des;
    }

    @Override
    public String toString() {
        return this.des;
    }
}
