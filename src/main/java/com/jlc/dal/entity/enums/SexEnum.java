package com.jlc.dal.entity.enums;

/**
 * 性别枚举
 */
public enum SexEnum {

    MAN("男"),
    WOMAN("女"),
    SECRET("保密");

    private String des;

    SexEnum(String des){
        this.des=des;
    }

    @Override
    public String toString() {
        return this.des;
    }
}
