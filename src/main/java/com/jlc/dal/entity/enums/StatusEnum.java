package com.jlc.dal.entity.enums;

/**
 * 通用状态定义
 */
public enum StatusEnum {

    ON("上架"),
    OFF("下架");

    private String des;

    StatusEnum(String des){
        this.des=des;
    }

    @Override
    public String toString() {
        return this.des;
    }
}
