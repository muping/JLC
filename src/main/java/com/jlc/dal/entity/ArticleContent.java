package com.jlc.dal.entity;

import java.util.Date;

public class ArticleContent {
    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column article_content.id
     *
     * @mbg.generated Sat Apr 14 22:17:00 CST 2018
     */
    private Long id;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column article_content.article_id
     *
     * @mbg.generated Sat Apr 14 22:17:00 CST 2018
     */
    private Long articleId;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column article_content.create_time
     *
     * @mbg.generated Sat Apr 14 22:17:00 CST 2018
     */
    private Date createTime;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column article_content.update_time
     *
     * @mbg.generated Sat Apr 14 22:17:00 CST 2018
     */
    private Date updateTime;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column article_content.content
     *
     * @mbg.generated Sat Apr 14 22:17:00 CST 2018
     */
    private String content;

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column article_content.id
     *
     * @return the value of article_content.id
     *
     * @mbg.generated Sat Apr 14 22:17:00 CST 2018
     */
    public Long getId() {
        return id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column article_content.id
     *
     * @param id the value for article_content.id
     *
     * @mbg.generated Sat Apr 14 22:17:00 CST 2018
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column article_content.article_id
     *
     * @return the value of article_content.article_id
     *
     * @mbg.generated Sat Apr 14 22:17:00 CST 2018
     */
    public Long getArticleId() {
        return articleId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column article_content.article_id
     *
     * @param articleId the value for article_content.article_id
     *
     * @mbg.generated Sat Apr 14 22:17:00 CST 2018
     */
    public void setArticleId(Long articleId) {
        this.articleId = articleId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column article_content.create_time
     *
     * @return the value of article_content.create_time
     *
     * @mbg.generated Sat Apr 14 22:17:00 CST 2018
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column article_content.create_time
     *
     * @param createTime the value for article_content.create_time
     *
     * @mbg.generated Sat Apr 14 22:17:00 CST 2018
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column article_content.update_time
     *
     * @return the value of article_content.update_time
     *
     * @mbg.generated Sat Apr 14 22:17:00 CST 2018
     */
    public Date getUpdateTime() {
        return updateTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column article_content.update_time
     *
     * @param updateTime the value for article_content.update_time
     *
     * @mbg.generated Sat Apr 14 22:17:00 CST 2018
     */
    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column article_content.content
     *
     * @return the value of article_content.content
     *
     * @mbg.generated Sat Apr 14 22:17:00 CST 2018
     */
    public String getContent() {
        return content;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column article_content.content
     *
     * @param content the value for article_content.content
     *
     * @mbg.generated Sat Apr 14 22:17:00 CST 2018
     */
    public void setContent(String content) {
        this.content = content == null ? null : content.trim();
    }
}