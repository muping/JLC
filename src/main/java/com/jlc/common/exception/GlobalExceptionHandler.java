package com.jlc.common.exception;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * 全局异常处理器
 */
@ControllerAdvice(annotations = {RestController.class})
public class GlobalExceptionHandler {

    private static Logger logger = LoggerFactory.getLogger(GlobalExceptionHandler.class);

    private static final String NO_CODE = "发生了未知错误！";

    @ExceptionHandler(BusinessException.class)
    @ResponseBody
    public ResponseResult doBusinessException(BusinessException e) {
        logger.error("BusinessException={}", e);
        //获取错误码
        String code = e.getMessage();
        //获取错误信息
        String msg = GlobalExceptionUtils.get(code);
        if (StringUtils.isEmpty(msg)) {
            return ResponseResult.generateErrorMsgResponseResult(NO_CODE);
        }
        return ResponseResult.generateErrorMsgResponseResult(msg);
    }

    @ExceptionHandler(Exception.class)
    @ResponseBody
    public ResponseResult doException(Exception e) {
        logger.error("Exception={}", e);
        //获取错误信息
        String msg = e.getMessage();
        if (StringUtils.isEmpty(msg)) {
            return ResponseResult.generateErrorMsgResponseResult(NO_CODE);
        }
        return ResponseResult.generateErrorMsgResponseResult(msg);
    }
}
