package com.jlc.common.exception;

/**
 * 业务异常
 */
public class BusinessException extends RuntimeException {

    public BusinessException(Integer code) {
        //将错误码传入
        super(code + "");
    }
}
