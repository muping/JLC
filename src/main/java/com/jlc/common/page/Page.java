package com.jlc.common.page;

import lombok.Getter;
import lombok.Setter;
import org.apache.ibatis.session.RowBounds;

/**
 * 封装分页信息
 */
@Getter
@Setter
public class Page extends RowBounds {

    /**
     * pageSize请求参数名
     */
    public static final String PAGE_SIZE_PARAM = "pageSize";
    /**
     * pageNo请求参数名
     */
    public static final String PAGE_NO_PARAM = "pageNo";

    /**
     * 默认分页大小
     */
    private static final Integer DEFAULT_PAGE_SIZE = 15;

    /**
     * 起始页
     */
    private static final Integer DEFAULT_PAGE_NO = 1;

    /**
     * 每页的大小
     */
    private Integer pageSize;
    /**
     * 当前页（从1开始）
     */
    private Integer pageNo;
    /**
     * 总页数
     */
    private Integer totalPage;
    /**
     * 总记录数
     */
    private Integer TotalCount;
    /**
     * 分页数据
     */
    private Object result;

    Page(int pageNo, int pageSize) {
        super((pageNo - 1) * pageSize, pageSize);
        this.pageNo = pageNo;
        this.pageSize = pageSize;
    }

    Page() {
        this(DEFAULT_PAGE_NO, DEFAULT_PAGE_SIZE);
    }

    public static Page generatePageResult(Object data) {
        Page page = PageContext.get();
        page.setResult(data);
        //计算totalPage
        int totalPage = page.getTotalCount() / page.getPageSize();
        if (page.getTotalCount() % page.getPageSize() != 0) {
            totalPage += 1;
        }
        page.setTotalPage(totalPage);
        return page;
    }
}
