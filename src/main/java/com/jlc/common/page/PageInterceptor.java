package com.jlc.common.page;

import com.jlc.common.utils.ReflectUtil;
import org.apache.ibatis.executor.parameter.ParameterHandler;
import org.apache.ibatis.executor.statement.StatementHandler;
import org.apache.ibatis.mapping.BoundSql;
import org.apache.ibatis.plugin.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;

/**
 * 分页拦截器
 */
@Intercepts({@Signature(method = "prepare", type = StatementHandler.class, args = {Connection.class, Integer.class})})
public class PageInterceptor implements Interceptor {

    /**
     * BoundSql里面的parameterObject只会有一个，就算mybatis动态代理时有多个参数，
     * 最终也会封装为map.然后调用sqlSession.selectList()
     * 因为selectList只支持一个参数
     */
    @Override
    public Object intercept(Invocation invocation) throws Throwable {
        Page page = PageContext.get();
        if (page == null) {
            //不需要分页
            return invocation.proceed();
        }
        StatementHandler statementHandler = (StatementHandler) invocation.getTarget();
        StatementHandler delegate = (StatementHandler) ReflectUtil.getFieldValue(statementHandler, "delegate");
        BoundSql boundSql = delegate.getBoundSql();
        //查询总记录数，并且封装到page中
        Connection connection = (Connection) invocation.getArgs()[0];
        ParameterHandler parameterHandler = (ParameterHandler) ReflectUtil.getFieldValue(delegate, "parameterHandler");
        setTotalCount(parameterHandler, page, boundSql, connection);
        //查询分页信息
        String sql = boundSql.getSql();
        sql = generateLimitSql(sql, page);
        ReflectUtil.setFieldValue(boundSql, "sql", sql);
        return invocation.proceed();
    }

    @Override
    public Object plugin(Object target) {
        return Plugin.wrap(target, this);
    }

    @Override
    public void setProperties(Properties properties) {
    }

    /**
     * 获取总的记录数
     *
     * @param boundSql   boundSql
     * @param connection 数据库连接
     */
    private void setTotalCount(ParameterHandler parameterHandler, Page page, BoundSql boundSql, Connection connection) {
        String sql = boundSql.getSql();
        sql = generateCountSql(sql);
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            pstmt = connection.prepareStatement(sql);
            parameterHandler.setParameters(pstmt);
            rs = pstmt.executeQuery();
            if (rs.next()) {
                int totalRecord = rs.getInt(1);
                page.setTotalCount(totalRecord);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null)
                    rs.close();
                if (pstmt != null)
                    pstmt.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 生成获取记录总数sql
     *
     * @param sql 原来的sql
     * @return 记录总数sql
     */
    private String generateCountSql(String sql) {
        int index = sql.indexOf("from");
        if (index == -1) {
            index = sql.indexOf("FROM");
        }
        return "SELECT COUNT(*) " + sql.substring(index);
    }

    /**
     * 生成分页sql
     *
     * @param sql  原来sql
     * @param page 分页信息
     * @return 分页sql
     */
    private String generateLimitSql(String sql, Page page) {
        return sql + " LIMIT " + page.getOffset() + " , " + page.getLimit();
    }
}
