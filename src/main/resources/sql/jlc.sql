CREATE TABLE `article` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `title` varchar(255) NOT NULL COMMENT '文章题目',
  `writer` bigint(20) NOT NULL COMMENT '作者，user_id',
  `cover_photo` varchar(255) NOT NULL COMMENT '封面图片url',
  `page_view` varchar(255) NOT NULL COMMENT '访问量',
  `status` varchar(255) NOT NULL COMMENT '文章状态，on和off',
  `category_id` bigint(20) NOT NULL COMMENT '分类id',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_time` datetime NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `article_content` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `article_id` bigint(20) NOT NULL COMMENT '文章主键',
  `content` longtext NOT NULL COMMENT '文章内容',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_time` datetime NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `category` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(255) NOT NULL COMMENT '分类名称',
  `parent_id` bigint(20) NOT NULL COMMENT '父分类',
  `status` varchar(255) NOT NULL COMMENT '分类状态，on和off',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_time` datetime NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `link` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `url` varchar(255) NOT NULL COMMENT '目标地址url',
  `title` varchar(255) NOT NULL COMMENT '目标地址标题',
  `des` varchar(255) NOT NULL COMMENT '目标地址描述',
  `status` varchar(255) NOT NULL COMMENT 'url状态，on和off',
  `category_id` bigint(20) NOT NULL COMMENT '分类id',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_time` datetime NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `user_name` varchar(255) NOT NULL COMMENT '登录用户名',
  `password` varchar(255) NOT NULL COMMENT '登录密码',
  `phone` varchar(255) NOT NULL DEFAULT '' COMMENT '手机号码，暂无作用，后期可以用来注册和登录',
  `sex` varchar(255) NOT NULL DEFAULT '' COMMENT '性别',
  `email` varchar(255) NOT NULL DEFAULT '' COMMENT '邮箱',
  `user_kind` varchar(255) NOT NULL COMMENT '用户类型，user和manager',
  `status` varchar(255) NOT NULL COMMENT '用户状态，normal和lock',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_time` datetime NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;